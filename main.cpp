#include <iostream>
#include "include/Board.h"

using namespace std;

void solve(Board* board, int row);
int numberOfSolves=0;

int main()
{
    cout << "Hello world!" << endl;
    Board *board = new Board();
    board->printBoard();
    solve(board, 0);
    cout << "Количество решений: " << numberOfSolves << endl;
    delete board;
    return 0;
}

void solve(Board *board, int row)
{
    if(row==8)
    {
        board->printBoard();
        numberOfSolves++;
    }
    int numberOfPossibles;
    int* places = board->possibles(row, numberOfPossibles);
    for(int j=0;j<numberOfPossibles;j++)
    {
        Board *newBoard = Board::duplicateBoard(board);
        newBoard->place(new Queen(row, places[j]));
        solve(newBoard, row+1);
    }
}
