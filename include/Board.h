#ifndef BOARD_H
#define BOARD_H
#include "Queen.h"
#include <iostream>
#include <string>
#include <fstream>
using namespace std;

class Board
{
public:
        Board();
        ~Board();
        bool isUnderAttack(int x, int y);
        int* possibles(int row, int& numberOfPossibles);
        void place(Queen *queen);
        bool isRowFree(int row)
        {
            return !rows[row];
        }
        static Board* duplicateBoard(Board *oldBoard);
        int** getMatrix();
        void setMatrix(int** matrix){this->matrix = matrix;}
        int getSize(){return size;}
        bool* getRows();
        void setRows(bool* rows){this->rows=rows;}
        void printBoard();
        void printToFile();

protected:
private:
    int** matrix;
    int size;
    bool *rows;
    static string outputFile;
    static ofstream outStream;
};

#endif // BOARD_H
