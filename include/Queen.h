#ifndef QUEEN_H
#define QUEEN_H


class Queen
{
public:
    Queen();
    Queen(int x, int y);
    virtual ~Queen();
    int getX();
    int getY();
protected:
private:
    int x, y;
};

#endif // QUEEN_H
