#include "../include/Board.h"

Board::Board()
{
    size = 8;
    matrix = new int*[size];
    for(int i=0; i<size; i++)
    {
        matrix[i] = new int[size];
    }
    rows = new bool[size];
}

Board::~Board()
{
    for(int i=0; i<size; i++)
    {
        delete[] matrix[i];
    }
    delete[] matrix;
    delete[] rows;
    outStream.close();
}

bool Board::isUnderAttack(int x, int y)
{
    return matrix[x][y] != 0;
}

int* Board::possibles(int row, int& numberOfPossibles)
{
    numberOfPossibles = 0;
    if(row > 7 || row < 0)
    {
        return new int[0];
    }
    int* result = new int[size];
    for(int i=0; i<size; i++)
    {
        result[i] = -1;
    }
    for(int i=0; i<size; i++)
    {
        if(!isUnderAttack(row, i))
        {
            result[numberOfPossibles++]=i;
        }
    }
    return result;
}

void Board::place(Queen *queen)
{
    int x=queen->getX();
    int y=queen->getY();
    if(!isUnderAttack(x, y))
    {
        for(int i=0; i<size; i++)
        {
            matrix[x][i]=1;
            matrix[i][y]=1;
        }
        matrix[x][y] = 2;
        while(x>0 && y>0)
        {
            matrix[--x][--y]=1;
        }
        x=queen->getX();
        y=queen->getY();
        while(x>0 && y<7)
        {
            matrix[--x][++y]=1;
        }
        x=queen->getX();
        y=queen->getY();
        while(y<7 && x<7)
        {
            matrix[++x][++y]=1;
        }
        x=queen->getX();
        y=queen->getY();
        while(x<7 && y>0)
        {
            matrix[++x][--y]=1;
        }
    }
}

Board* Board::duplicateBoard(Board *oldBoard)
{
    Board *newBoard = new Board();
    newBoard->setMatrix(oldBoard->getMatrix());
    newBoard->setRows(oldBoard->getRows());
    return newBoard;
}

int** Board::getMatrix()
{
    int** result = new int*[size];
    for(int i=0; i<size; i++)
    {
        result[i] = new int[size];
        for(int j=0; j<size; j++)
        {
            result[i][j] = matrix[i][j];
        }
    }
    return result;
}

bool* Board::getRows()
{
    bool* result = new bool[size];
    for(int i=0; i<size; i++)
    {
        result[i] = rows[i];
    }
    return result;
}

void Board::printBoard()
{
    cout << "///////////////////////////////" << endl;
    for(int i=0; i<size; i++)
    {
        for(int j=0; j<size; j++)
        {
            cout << matrix[i][j] << " ";
        }
        cout << endl;
    }
    cout << "///////////////////////////////" << endl;
    printToFile();
}

string Board::outputFile = "output_queen.txt";
ofstream Board::outStream(outputFile.c_str(), ios_base::app);

void Board::printToFile()
{
    outStream << "Решение" << endl;
    for(int i=0;i<size;i++)
    {
        for(int j=0;j<size;j++)
        {
            if(matrix[i][j] == 2)
            {
                outStream << i << " " << j << endl;
            }
        }
    }
}
